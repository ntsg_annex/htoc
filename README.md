**file:** README.md

**revised:** 2016-07-13T15:38:00

**author:** Joe Glassy

**purpose:** Document NTSG utility, "htoc.py", a python utility to read an HDF5 file and
         produce a name-value-pair style full table of contents (.toc) text file.
         
**version:** 1.4

**status:** stable

**dependencies:** Python 2.7, and python modules: os,sys, time, datetime, uuid, random, math,
         hashlib, numpy, h5py
         
**notes:** Several different operational modes are supported, as per invocation options below,
            noting that simple scalar attribute values are also output along with the attribute name.
            
**invocation:** `python2.7 htoc.py <input:MyAncillaryFile.h5> <options:{FULL|BRIEF}> <OUTPUT-UserNamed.toc>`

**example:** `python2.7 htoc.py  MyAncillaryFile.h5  BRIEF  MyAncillaryFile.toc`

**notes:** This utility has been tested primarily on NASA SMAP Level 4 Carbon granule file
         formats in HDF5. It has not been tested on HDF5 files that contain features
         or characteristics beyond those used in the aforementioned file
**notes:** A new version (v1.5.x) is in preparation, which produces special output using a "|"
         delimiter for the attribute names. Especially useful for Freeze Thaw metadata preparation.